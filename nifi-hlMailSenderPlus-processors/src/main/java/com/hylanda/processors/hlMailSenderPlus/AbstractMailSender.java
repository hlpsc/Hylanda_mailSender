package com.hylanda.processors.hlMailSenderPlus;

import org.apache.nifi.components.AllowableValue;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.components.Validator;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.util.StandardValidators;

import java.util.List;
import java.util.Set;

/**
 * Created by GuoMeng on 2019/1/4.
 */
public abstract class AbstractMailSender extends AbstractProcessor {
    /**
     * SMTP设置
     */
    public static final PropertyDescriptor SMTP_SERVER  = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.SMTP_SERVER )
                                                            .displayName( "SMTP 地址" )
                                                            .description( "不用填写SMTP://" )
                                                            .required( true )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();
    public static final PropertyDescriptor SMTP_SSL     = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.SMTP_SSL )
                                                            .displayName( "ssl" )
                                                            .description( "使用SSL协议发送邮件" )
                                                            .required( true )
                                                            .defaultValue( "0" )
                                                            .allowableValues(
                                                                    new AllowableValue( "0", "否" ),
                                                                    new AllowableValue( "1", "是" )
                                                            )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();
    public static final PropertyDescriptor SMTP_PORT    = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.SMTP_PORT )
                                                            .displayName( "SMTP 端口号" )
                                                            .description( "默认端口号：25" )
                                                            .required( true )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();
    public static final PropertyDescriptor SMTP_USER    = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.SMTP_USER )
                                                            .displayName( "SMTP 账号" )
                                                            .description( "" )
                                                            .required( true )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();
    public static final PropertyDescriptor SMTP_PWD     = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.SMTP_PWD )
                                                            .displayName( "SMTP 密码" )
                                                            .description( "" )
                                                            .required( true )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();


    /**
     * 发件设置
     */
    public static final PropertyDescriptor SEND_TYPE   = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.SEND_TYPE )
                                                            .displayName( "发送方式" )
                                                            .description( "时间间隔：每X小时发送一次，(单位小时)；指定时间：每天到达指定时间发送一次，使用','分隔多个时间点（如：8,16,24)；数据量：数据量每达到预设阈值发送。" )
                                                            .required( true )
                                                            .defaultValue( "timer" )
                                                            .allowableValues(
                                                                    new AllowableValue( "timer", "指定时间" ),
                                                                    new AllowableValue( "timeout", "时间间隔" ),
                                                                    new AllowableValue( "num", "数据量" )
                                                            )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();
    public static final PropertyDescriptor SEND_TYPEV  = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.SEND_TYPEV )
                                                            .displayName( "" )
                                                            .description( "发送设置" )
                                                            .required( true )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();

    public static final PropertyDescriptor SEND_TO      = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.SEND_TO )
                                                            .displayName( "收件人" )
                                                            .description( "多个邮件地址使用','分隔" )
                                                            .required( true )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();
    public static final PropertyDescriptor SEND_CC      = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.SEND_CC )
                                                            .displayName( "抄送地址" )
                                                            .description( "多个邮件地址使用','分隔" )
                                                            .required( false )
                                                            .addValidator( Validator.VALID ).build();
    public static final PropertyDescriptor SEND_BCC     = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.SEND_BCC )
                                                            .displayName( "密送地址" )
                                                            .description( "多个邮件地址使用','分隔" )
                                                            .required( false )
                                                            .addValidator( Validator.VALID ).build();

    /**
     * 内容设置
     */
    public static final PropertyDescriptor MAIL_SUBJECT = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.MAIL_SUBJECT )
                                                            .displayName( "邮件主题" )
                                                            .description( "参考帮助文档中的主题模板规范" )
                                                            .required( true )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();
    public static final PropertyDescriptor MAIL_MESSAGE  = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.MAIL_MESSAGE )
                                                            .displayName( "邮件内容" )
                                                            .description( "参考帮助文档中的主题模板规范" )
                                                            .required( true )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();
    public static final PropertyDescriptor MAIL_FILTER = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.MAIL_FILTER )
                                                            .displayName( "消重字段" )
                                                            .description( "仅支持单字段过滤" )
                                                            .required( false )
                                                            .addValidator( Validator.VALID ).build();
    public static final PropertyDescriptor MAIL_COUNT   = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.MAIL_COUNT )
                                                            .displayName( "统计设置" )
                                                            .description( "使用JSON格式进行配置，tag:统计字段，val:统计值（多个值使用半角“,”分隔，统计全部内容使用“*”），pri:需要高亮的取值（多个值使用半角“,”分隔）" )
                                                            .required( false )
                                                            .addValidator( Validator.VALID ).build();
    public static final PropertyDescriptor MAIL_LIMIT   = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.MAIL_LIMIT )
                                                            .displayName( "正文数据条数" )
                                                            .description( "-1表示全部发送" )
                                                            .required( true )
                                                            .defaultValue( "-1" )
                                                            .addValidator( StandardValidators.INTEGER_VALIDATOR ).build();
    public static final PropertyDescriptor MAIL_ATTR    = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.MAIL_ATTR )
                                                            .displayName( "附件类型" )
                                                            .description( "当前仅支持EXCEL" )
                                                            .required( true )
                                                            .defaultValue( "excel" )
                                                            .allowableValues(
                                                                new AllowableValue( "none", "无" ),
                                                                new AllowableValue( "excel", "EXCEL" )
                                                            )
                                                            .addValidator( StandardValidators.NON_EMPTY_VALIDATOR ).build();
    public static final PropertyDescriptor MAIL_OUTPUT  = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.MAIL_OUTPUT )
                                                            .displayName( "输出字段" )
                                                            .description( "附件内容中包括的字段列表，使用半角“,”分隔，默认全部输出" )
                                                            .required( false )
                                                            .addValidator( Validator.VALID ).build();
    public static final PropertyDescriptor MAIL_ATTR_ORDER = new PropertyDescriptor.Builder()
                                                            .name( MailConfig.MAIL_ATTR_ORDER )
                                                            .displayName( "附件排序字段" )
                                                            .description( "" )
                                                            .required( false )
                                                            .addValidator( Validator.VALID ).build();



    public static final Relationship MY_RELATIONSHIPOK = new Relationship.Builder()
                                                            .name( "成功" )
                                                            .description( "发送成功数据" )
                                                            .autoTerminateDefault(true)
                                                            .build();

    public static final Relationship MY_RELATIONSHIPNO = new Relationship.Builder()
                                                            .name( "失败" )
                                                            .description( "发送失败数据" )
                                                            .autoTerminateDefault(true)
                                                            .build();

    protected List<PropertyDescriptor> descriptors;
    protected Set<Relationship> relationships;

    public void outputError(String msg) {
        getLogger().error(msg);
    }
}
