package com.hylanda.processors.hlMailSenderPlus;

/**
 * Created by GuoMeng on 2019/1/10.
 */
public class MailTpl {
    public static String def   = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
            "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">\n" +
            "<head>\n" +
            "    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">\n" +
            "    <title>Document</title>\n" +
            "    <style type=\"text/css\">\n" +
            "        *{ font-family: \"微软雅黑\"; font-size:12px; }\n" +
            "        body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote,th,td { margin:0; padding:0; }\n" +
            "        table { border-collapse:collapse; border-spacing:0; }\n" +
            "        fieldset,img { border:0; }\n" +
            "        address,caption,cite,code,dfn,em,strong,th,var { font-style:normal; font-weight:normal; }\n" +
            "        ol,ul { list-style:none; }\n" +
            "        caption,th { text-align:left; }\n" +
            "        h1,h2,h3,h4,h5,h6 { color:#25549e; font-weight:normal; }\n" +
            "        q:before,q:after { content:''; }\n" +
            "        abbr,acronym { border:0; }\n" +
            "\n" +
            "        .main, .count{ width:640px; margin: 0 auto; border:solid 1px #666; padding:10px 20px; }\n" +
            "        .main > div{width:542px;margin-bottom: 25px;border-bottom: 1px solid #dedede;padding-bottom: 10px}\n" +
            "        .main > div h4 a{font-size: 16px;color: #25549e;text-decoration: none;font-weight: bold;width:542px;display: inline-block;}\n" +
            "        .main > div h4 a:hover{text-decoration: underline;}\n" +
            "        .main > div h4 a:visited{color: #000;}\n" +
            "        .main > div p{font-size: 12px;color:#474747;height:auto;max-height:59px;margin-top:5px;overflow:hidden;}\n" +
            "        .main > div > div{height: auto;margin-top:3px;clear: both;overflow:hidden}\n" +
            "        .main > div > div span{ font-size: 12px;color:#909090; margin-right: 10px; float:left; }\n" +
            "\n" +
            "        .main > div > h4{font-size: 14px;font-weight: bold;}\n" +
            "        .main > div >  p a:hover{text-decoration: underline;}\n" +
            "        .main > div >  p a:visited{color:#747474;}\n" +
            "        .main > div >  p a{text-decoration: none; color: #25549e;height:auto;max-height:59px;overflow: hidden; }\n" +
            "\n" +
            "        .count table{ width:100%; border:solid 1px #000; }\n" +
            "        .count table thead{ border-bottom:solid 2px #333; }\n" +
            "        .count table td, .count table th{ border-right:solid 1px #666; border-bottom:solid 1px #666; text-align:left; }\n" +
            "        .count table tr.hl > *{ color:red; }\n" +
            "        .count table th{ font-weight: 600; }\n" +
            "\n" +
            "    </style>\n" +
            "</head>\n" +
            "<body>\n" +
            "    <div class=\"warpper\">\n" +
            "        <div class=\"count\"><h1>数据总览</h1>%count%</div>\n" +
            "        <div class=\"main\"><h1>数据样例（最多显示%numlimit%条）</h1>%maildata%</div>\n" +
            "        <p>具体详情请查看附件内容。</p>\n" +
            "    </div>\n" +
            "</body>\n" +
            "</html>";
}
