## 能力说明
    用于将数据作为邮件的内容发送到指定的邮箱

### 使用DEMO
![输入图片说明](https://images.gitee.com/uploads/images/2019/0111/155644_52819901_1730330.png "屏幕截图.png")

### 配置参数
#### SMTP服务部分
|参数名称|示例|说明|
| -- | -- | -- |
|SMTP 地址|smtp.163.com|SMTP服务器地址|
|SMTP 端口号|25|SMTP服务器端口号|
|ssl|是|是否启用SSL协议 **部分SMTP服务会对非SSL的请求进行限制，建议启用该配置** |
|SMTP 账号|xxxx@hylanda.com|SMTP密码|
|SMTP 密码|xxxx|SMTP密码|


#### 邮件发送部分
|参数名称|示例|说明|
| -- | -- | -- |
|发送方式|数据量|发送方式，时间间隔：每X小时发送一次，(单位小时)；指定时间：每天到达指定时间发送一次，使用半角','分隔多个时间点（如：8,16,24)；数据量：数据量每达到预设阈值发送。|
|收件人|aaa@hylanda.com,bbb@hylanda.com|收件人地址，多个收件人使用半角','分隔|
|抄送地址|aaa@hylanda.com,bbb@hylanda.com|抄送地址，多个收件人使用半角','分隔|
|密送地址|aaa@hylanda.com,bbb@hylanda.com|密送地址，多个收件人使用半角','分隔|
|正文数据条数|20|在邮件的正文中默认发送的(消重后)数据条目数|
| -- | -- | -- | -- | -- | -- |
|邮件主题|详见下文|邮件主题模板|
|邮件内容|详见下文|邮件主题模板|
|消重字段|download_date|邮件在发送之前会根据该字段进行消重，注意：每次消重只针对本轮邮件发送中的数据范围。|
|统计设置|media_name:author|生成统计表格的统计字段，详见下文|
|附件类型|excel|用于将（未消重的）全部数据作为邮件的附件进行发送|

#### 邮件标题模板
    模板基本形式，例如：达能 %NUMALL%条数据，消重后%NUMREAL%条 %DATAMIN_download_time% - %DATAMAX_download_time%
%TAG%的形式为一个“转换标签”

1.主题可用“转换标签”说明：

    %NUMALL% 合计条数
    %NUMREAL% 实际条数（消重后）
    %DATAMAX_download_time% 指定字段的最小值（例子中为下载时间的最小值）
    %DATAMIN_download_time% 指定字段的最大值（例子中为下载时间的最大值）
        该配置必须为2部分组成：“DATA[筛选方式]_”前缀，字段名称。
        字段名称：必须为实际可以获取到的字段，且区分大小写
        字段排序的方式按照JAVA字符串排序进行

2.邮件内容模板

    模板基本形式，例如：

        <div><h3>%title%</h3><p><a href='%link%' target='blank'>%content%</a></p><p>%donwload_data% | 来源:%from%</p><p>标签:%tag% 特征词：%keyword%</p></div>

    用于使用该HTML格式循环输出每个匹配的数据到邮件的正文：

![输入图片说明](https://images.gitee.com/uploads/images/2019/0111/162451_9da509d4_1730330.png "屏幕截图.png")

    每个“转换标签”必须与上游数据源中的字段完全匹配

3.统计字段说明    
    
    统计字段设置支持两种形式
        一维统计：直接设置统计字段

![输入图片说明](https://images.gitee.com/uploads/images/2019/0111/162429_147add15_1730330.png "屏幕截图.png")


        二维统计：第一个统计维度字段:第二个统计维度字段

![输入图片说明](https://images.gitee.com/uploads/images/2019/0111/162356_5592e9fb_1730330.png "屏幕截图.png")
     


