package com.hylanda.processors.hlMailSenderPlus;

import com.alibaba.fastjson.JSONObject;
import org.apache.nifi.processor.ProcessContext;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by GuoMeng on 2019/1/4.
 */
public class MailConfig{
    protected hlMailSenderProcessor mHlMailSenderProcessor;

    public static String SMTP_SERVER    = "SMTP_SERVER";
    public static String SMTP_PORT      = "SMTP_PORT";
    public static String SMTP_USER      = "SMTP_USER";
    public static String SMTP_PWD       = "SMTP_PWD";
    public static String SMTP_SSL       = "SMTP_SSL";
    public static String SEND_TYPE      = "SEND_TYPE";
    public static String SEND_TYPEV     = "SEND_TYPEV";
    public static String SEND_TO        = "SEND_TO";
    public static String SEND_CC        = "SEND_CC";
    public static String SEND_BCC       = "SEND_BCC";
    public static String MAIL_LIMIT     = "MAIL_LIMIT";
    public static String MAIL_SUBJECT   = "MAIL_SUBJECT";
    public static String MAIL_MESSAGE   = "MAIL_MESSAGE";
    public static String MAIL_COUNT     = "MAIL_COUNT";
    public static String MAIL_ATTR      = "MAIL_ATTR";
    public static String MAIL_OUTPUT    = "MAIL_OUTPUT";
    public static String MAIL_ATTR_ORDER= "MAIL_ATTR_ORDER";
    public static String MAIL_FILTER    = "MAIL_FILTER";
    public static String DATA_STAT      = "DATA_STAT";

    public static String SMTP_TIMEOUT   = "10000";

    protected static Long lastSendTime  = ( long )0;
    protected static String lastSendRst = "";

    protected HashMap<String, String> config;


    public MailConfig( ProcessContext processContext, hlMailSenderProcessor _hlMail_senderProcessor) throws Exception{
        mHlMailSenderProcessor = _hlMail_senderProcessor;
        config      = new HashMap<>();
        Field[] _aF = this.getClass().getDeclaredFields();
        for( Field _f : _aF ){
            if( _f.getName() == "config" )
                break;
            if( processContext == null )
                config.put( _f.getName(), "" );
            else
                config.put( _f.getName(), String.valueOf( processContext.getProperty( _f.getName() ).getValue() ) );
        }

        MailService service     = new MailService( this, _hlMail_senderProcessor);
        service.test();
    }

    public String getLastSend(){
        return ( new SimpleDateFormat( "yyyy-MM-dd  HH:mm:ss" ).format( lastSendTime ) ) + " " + lastSendRst;
    }

    public void setLastSend( Long time ){
        lastSendTime    = time;
    }

    public String toString(){
        return JSONObject.toJSONString( config );
    }

    public String getValue( String sKey ){
        return config.containsKey( sKey ) ? config.get( sKey ) : null;
    }

    public void sendMail( MailData oData ) throws Exception{
        boolean sendMail    = false;
        String sendType     = getValue( MailConfig.SEND_TYPE );
        String sendValue    = getValue( MailConfig.SEND_TYPEV );
        switch( sendType ){
            /*定点发送*/
            case "timer"    :
                String _curTime         = new SimpleDateFormat( "HH:mm").format( new Date() );
                List<String> sendTimer  = Arrays.asList( sendValue.split( "," ) );
                /*检测当前系统时间是否为发送邮件的时间段*/
                for( String _t : sendTimer ){
                    if( _curTime.startsWith( _t ) ){
                        /*之前没有发送记录*/
                        if( lastSendTime == 0 ) {
                            sendMail = true;
                            break;
                        }else{
                            /*之前有过发送记录，需要检测发送时间是否也在当前的时间段*/
                            String _befHour     = new SimpleDateFormat( "HH:mm").format( lastSendTime );
                            if( !_befHour.startsWith( _t ) ){
                                sendMail        = true;
                                break;
                            }
                        }
                    }
                }
                break;
            /*定时发送*/
            case "timeout"  :
                sendMail    = System.currentTimeMillis() > lastSendTime + Integer.valueOf( sendValue ) * 3600 * 1000  ? true : false;
                break;
            /*按数量发送*/
            case "num"      :
                sendMail    = oData.count() >= Integer.valueOf( sendValue ) ? true : false;
                if( sendMail && lastSendTime != 0 ){
                    /*防止发送过快，定量发送最快每分钟发送一次*/
                    if( System.currentTimeMillis() < lastSendTime + 60000 )
                        sendMail    = false;
                }
                break;
        }

        if( sendMail ){
            /*发送邮件*/
            mHlMailSenderProcessor.writeLog( "START MAIL SENDING..." );
            MailService service     = new MailService( this, mHlMailSenderProcessor );
            service.send( oData );
            mHlMailSenderProcessor.writeLog( "END MAIL SENDING..." );
        }
    }

    public void setLastSendTime( String msg ){
        lastSendTime    = System.currentTimeMillis();
        lastSendRst     = null == msg ? "" : msg;
    }
}
